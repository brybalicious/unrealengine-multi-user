#!/bin/sh

export PATH=$PATH:/usr/local/bin

set -e
set -x # Debug

# Create logging directory if it does not exist
if [ ! -d /var/lib/tailscale ]; then
    mkdir -p /var/lib/tailscale
fi

# Create networking device folder if it does not exist
if [ ! -d /dev/net ]; then
    mkdir -p /dev/net
fi

# Create tunnel adapter if it does not exist
if [ ! -c /dev/net/tun ]; then
    mknod /dev/net/tun c 10 200
fi

# Start the daemon
tailscaled --state=/state/tailscaled.state &

# Let it get connected to the control plane
sleep 10

# Start the interface with UP_FLAGS otherwise defaulting to whatever comes after :-
# https://docs.docker.com/engine/reference/builder/#environment-replacement
# Does not work, as the UP_FLAGS environment variable is not updated as its substitution is updated. See Dockerfile
# tailscale up ${UP_FLAGS:-}
tailscale up -authkey ${TAILSCALE_KEY}

# Command substitution to run UnrealMultiUser passed from CMD instruction in Dockerfile
# $1

# However, the CMD or ENTRYPOINT instructions in exec form ["json..."]) do not perform variable substitution ($)... so the following at least works -
# to place the UnrealMultiUserServer command here directly, and to pass any extra runtime flags as arguments provided by the CMD instruction via $1
UnrealMultiUserServer -UDPMESSAGING_TRANSPORT_UNICAST=$(tailscale status -json true | jq -r .Self.TailAddr):${PORT} $1

# Another option could be to use this script as a runtime script from CMD instruction instead of ENTRYPOINT (use ENTRYPOINT ["/bin/sh", "-c"])
# thus choose whether to e.g. run this script when desiring tailscale, or to simply run UnrealMultiUserServer with a different IP and flags directly as a CMD instruction
# On second thought, best to keep as it is, and override the entrypoint if desired