# syntax = docker/dockerfile:1.0-experimental
# above line tells docker to use the syntax required for buildkit --secret flag

# See for reference
# https://docs.docker.com/engine/reference/builder/
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/

# Download base image - https://hub.docker.com/_/ubuntu referring to https://cloud-images.ubuntu.com/minimal/releases/
# FROM ubuntu-minimal:focal
FROM ubuntu:latest

# Container Info
LABEL version=0.0.1
LABEL maintainer="brybalicious"
LABEL version=$version
LABEL description="Lightweight Unreal Multi-User dedicated server image running on tailscale"
LABEL unrealversion=4.26.1

# ENV or ARG - https://tailscale.com/kb/1080/cli and https://docs.docker.com/engine/reference/builder/#using-arg-variables i.e. build-time vs run-time variables)
# Build-time Variables
# https://github.com/moby/moby/issues/4032
ARG DEBIAN_FRONTEND=noninteractive

ENV PATH_TO_USER=/home/ubuntu
ENV PATH_TO_DATA=${PATH_TO_USER}/ubuntu/data
ENV PATH_TO_UNREAL=${PATH_TO_DATA}/UnrealEngine
ENV PATH_TO_CONFIG=${PATH_TO_UNREAL}/Engine/Config
ENV PATH_TO_MULTIUSER=${PATH_TO_UNREAL}/Engine/Binaries/Linux
ENV PATH=${PATH_TO_MULTIUSER}:$PATH

ENV DEFAULTSESSIONRESTORE=UEMultiUserSessionData
ENV PORT=7000

# Update Ubuntu first, and install some packages, as we will need e.g. curl before we can add tailscale repository
RUN apt-get update -qy \
    && apt-get upgrade -qy \
    && apt-get install -qy \
    build-essential \
    curl \
    git \
    jq

# Add Tailscale's GPG key & add the tailscale repository
# Ensure latest tailscale matches with ubuntu:latest - https://pkgs.tailscale.com/stable/
RUN curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/focal.gpg | apt-key add - \
    && curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/focal.list | tee /etc/apt/sources.list.d/tailscale.list

# Install Tailscale & clean apt
RUN apt-get update -qy \
    && apt-get install -qy tailscale \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

# Now, download and unzip Unreal Engine to /home/ubuntu/data
WORKDIR ${PATH_TO_USER}
RUN mkdir data
WORKDIR ${PATH_TO_DATA}

# SECRET - pass secrets at build time from credential file to authenticate git and clone Unreal Engine from private repository
# https://docs.docker.com/develop/develop-images/build_enhancements/#new-docker-build-secret-information
# https://github.blog/2020-12-15-token-authentication-requirements-for-git-operations/
RUN --mount=type=secret,id=credentials,dst=/credentials \
    git clone --depth=1 https://$(jq -r .git.username /credentials):$(jq -r .git.pat /credentials)@github.com/EpicGames/UnrealEngine.git

# Build Unreal Engine and UnrealMultiUserServer
WORKDIR ${PATH_TO_UNREAL}
RUN ./Setup.sh \
    && ./GenerateProjectFiles.sh \
    && make UnrealMultiUserServer

# Copy desired .ConsoleVariables.ini over .Engine/Config/ConsoleVariables.ini at docker build time
COPY /ConsoleVariables.ini ${PATH_TO_CONFIG}/ConsoleVariables.ini

# Expose UDP port from container to host
EXPOSE ${PORT}/udp

# Order of the following matters. Tailscale key reference - https://tailscale.com/kb/1068/acl-tags#pre-authenticated-keys
# Doesn't work with runtime ENV replacement with `docker run -e TAILSCALE_KEY`, as UP_FLAGS is not updated when TAILSCALE_KEY is updated
ENV TAILSCALE_KEY=""
# ENV UP_FLAGS="-authkey ${TAILSCALE_KEY}"

# Copy entrypoint script to container image at build time
COPY /docker-entrypoint.sh /usr/local/bin
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

# Now we need to run tailscale, and run the UnrealMultiUserServer... Works only after the ENTRYPOINT instruction i.e. run stage not build stage,
# as tailscale requires kernel privileges or --cap-add=NET_ADMIN and TUN device mounting
# https://github.com/tailscale/tailscale/blob/main/Dockerfile
# https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities
# https://github.com/tailscale/tailscale/issues/504 
# https://github.com/deasmi/unraid-tailscale

# OPTION 1 - go live with tailscale
# Run entrypoint script
ENTRYPOINT ["docker-entrypoint.sh"]
# Pass extra command flags to UnrealMultiUserServer as a single string - https://docs.unrealengine.com/en-US/ProductionPipelines/MultiUserEditing/Reference/index.html
CMD ["-ConcertSaveSessionAs=UEMultiUserSessionData"]

# OPTION 2 - go live without tailscale
# ENTRYPOINT ["/bin/sh", "-c"]
# CMD ["UnrealMultiUserServer -UDPMESSAGING_TRANSPORT_UNICAST=${SERVERIP}:${PORT} -ConcertSaveSessionAs=${DEFAULTSESSIONRESTORE}"]

# Note - must implement a method to find and substitute in the server ip
# Can override the entrypoint script by passing --entrypoint flag to `docker run`

# Check Dockerfile implementation once more - https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#leverage-build-cache

# Typical Usages:
# docker build --no-cache --progress=plain --secret id=credentials,src=credentials.json -t user/repo:tag .
# docker run -dit -p 7000:7000/udp --cap-add=NET_ADMIN --device=/dev/net/tun:/dev/net/tun -v /var/lib:/var/lib --env TAILSCALE_KEY="tskey-..." --hostname umuserver user/repo:tag