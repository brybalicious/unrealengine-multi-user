# UnrealEngine Multi-User

Dockerfile and images for building and launching Unreal Engine's Multi-User dedicated server on e.g. a cloud VM such as Google Cloud Compute Engine or Amazon EC2

## Inspirational Documentation

- [Setting up an Unreal Engine Multi-User Server in the cloud](https://cdn2.unrealengine.com/unreal-engine-multi-user-cloud-setup-360515954.pdf)
- [Remote Multi-User Editing - Unreal Engine](https://cdn2.unrealengine.com/unreal-engine-remote-multi-user-editing-guide-404332864.pdf)
- [Multi-User Editing | Unreal Engine Documentation](https://docs.unrealengine.com/en-US/Engine/Editor/MultiUser/index.html)

- [UnrealContainers](https://unrealcontainers.com/)
- [ue4-docker](https://docs.adamrehn.com/ue4-docker/read-these-first/introduction-to-ue4-docker)

                freedom to create in cyberspace
